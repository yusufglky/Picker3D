using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public bool Punch;
    public Vector3 punch;
    public float duration;
    public int vibrato = 10;
    public float elasticity = 1;
    public bool snapping = false;

    void Update()
    {
        if (Punch)
        {
            Punch = false;
            transform.DOPunchPosition(punch, duration, vibrato, elasticity,  snapping );
        }
    }
}
