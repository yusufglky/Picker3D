using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Picker",menuName ="Picker/New Picker")]
public class PickerType:ScriptableObject
{
    public float VerticalSpeed;
    public float HorizontalSpeed;
}
