using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Platform",menuName ="Platforms/New Platform")]
public class PlatformType : ScriptableObject
{
    public int platformAccessObjectCounter;
    public float platformActivateTimer;
}
