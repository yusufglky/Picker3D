using DG.Tweening;
using UnityEngine;

[ExecuteInEditMode]
public class Barriers : MonoBehaviour
{
    [Header("Barriers")]
    [SerializeField] private Transform leftBarrierHolder;
    [SerializeField] private Transform rightBarrierHolder;

    [Header("Section")]
    [SerializeField] private Section mySection;
    [SerializeField] private GameObject myStopper;

    [Header("Anim Speed")]
    [SerializeField] private float openSpeed;

    private void OnEnable()
    {
        mySection.SectionSuccessAction += SectionSuccess;
    }
    private void OnDisable()
    {
        mySection.SectionSuccessAction -= SectionSuccess;
    }
    private void SectionSuccess()
    {
        OpenBarriers();
        StopperState(false);
    }
    private void OpenBarriers()
    {
        leftBarrierHolder.DOLocalRotate(new Vector3(0, 45, 0),openSpeed,RotateMode.LocalAxisAdd);
        rightBarrierHolder.DOLocalRotate(new Vector3(0, 45, 0),openSpeed, RotateMode.LocalAxisAdd);
    }
    private void StopperState(bool open)
    {
        myStopper.SetActive(open);
    }
}
