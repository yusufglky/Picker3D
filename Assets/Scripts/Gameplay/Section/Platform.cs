using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private PlatformType platformType;
    [SerializeField] private Section mySection;

    [Header("Objects")]
    private List<GameObject> _registeredObjects=new List<GameObject>();

    [Header("UI")]
    [SerializeField] private TextMeshPro platformCounterText;

    [Header("Timer")]
    private bool _timerStart;

    [Header("Anim")]
    [SerializeField] private float animDuration;
    private void Awake()
    {
        Setup();
    }
    private void Setup()
    {
        platformCounterText.text = _registeredObjects.Count + "/" + platformType.platformAccessObjectCounter;

    }
    private void Update()
    {
        PlatformActivator();
    }
    private void PlatformActivator()
    {
        if (Mathf.Abs(transform.position.z - Gamemanager.Singleton.PickerTransform.position.z) <= 10 && !_timerStart)
        {
            _timerStart = true;
            StartCoroutine(IE_PlatformActivateTimer());
        }
    }
    private void RegisterObject(GameObject obj)
    {
        if (!obj.CompareTag("CollectibleObjects"))
            return;
        if (!_registeredObjects.Contains(obj))
        {
            _registeredObjects.Add(obj);
            SetRegisteredObjRigidbody(obj.GetComponent<Rigidbody>());
            SetPlatformCounterText();
        }
    }
    private void SetRegisteredObjRigidbody(Rigidbody obj)
    {
        obj.GetComponent<Collider>().enabled = false;
        obj.constraints = RigidbodyConstraints.FreezeAll;
    }
    private void SetPlatformCounterText()
    {
        platformCounterText.text = _registeredObjects.Count + "/" + platformType.platformAccessObjectCounter;

        if (!DOTween.IsTweening(platformCounterText))
        {
            platformCounterText.DOColor(Color.red, 0.1f).OnComplete(() => platformCounterText.DOColor(Color.white, 0.1f));
        }
    }

    private void PlatformAccessible()
    {
        if (_registeredObjects.Count>=platformType.platformAccessObjectCounter)
        {
            SectionSuccess();
            Debug.Log("Platform Activated");
            //To Do: Platform Aktif Hale Gelsin
        }
        else
        {
            mySection.SectionState(false);
            Debug.Log("Platform Failed");
            //To Do: Platformu gecmek basarisiz (level Failed);
        }
    }
    private void SectionSuccess()
    {
        for (int i = 0; i < _registeredObjects.Count; i++)
        {
            _registeredObjects[i].GetComponent<Collider>().enabled = false;
            _registeredObjects[i].SetActive(false);
        }

        gameObject.layer = 11;

        platformCounterText.DOFade(0, animDuration);

        LocalColorChanger();
        transform.DOScale(new Vector3(10.4f, 2, 6), animDuration);
        transform.DOMoveY(0, animDuration)
            .OnComplete(() => transform.DOPunchPosition(Vector3.up * 0.45f, 0.35f, 1, 0.1f, false)
            .OnComplete(() => PlatformAnimationFinished()));
        //To Do Platformu yukselt ve hoplama efekti ver ve rengini degistir
    }
    private void PlatformAnimationFinished()
    {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        mySection.SectionState(true);
    }
    private void LocalColorChanger()
    {
        // MaterialPropertyBlock myBlock = new MaterialPropertyBlock();
        GetComponent<MeshRenderer>().SetPropertyBlock(null);
        GetComponent<MeshRenderer>().material.DOColor(LevelManager.Singleton.StartRoadColor.MainColor, animDuration);
       
       // myBlock.SetColor("_BaseColor", MainColor);
       // myBlock.SetColor("_EmissionColor", HdrColor);

       
    }
    private IEnumerator IE_PlatformActivateTimer()
    {
        yield return new WaitForSeconds(platformType.platformActivateTimer);
        PlatformAccessible();
    }
    private void OnCollisionEnter(Collision col)
    {
        RegisterObject(col.gameObject);
    }
}
