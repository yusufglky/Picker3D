using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Section : MonoBehaviour
{
    private SectionManager _sectionManager;
    public event Action SectionSuccessAction;
    public event Action SectionFailedAction;

    [SerializeField] private float minDistance=8;

    [SerializeField] private Platform myPlatform;
    public Platform MyPlatform { get { return myPlatform; } private set { myPlatform = value; }}
    private void Awake()
    {
        Setup();
    }
    private void Update()
    {
        PlayerDistance();
    }
    private void Setup()
    {
        _sectionManager = FindObjectOfType<SectionManager>();
        MyPlatform = GetComponentInChildren(typeof(Platform)) as Platform;
    }
    public void SectionState(bool sectionSuccess)
    {
        if (sectionSuccess)
        {
            SectionSuccessAction?.Invoke();
            _sectionManager.SectionFinished();
        }
        else
        {
            LevelManager.Singleton.LevelFailed();
        }
    }
    private void PlayerDistance()
    {
        if (Mathf.Abs(Gamemanager.Singleton.PickerTransform.position.z - transform.position.z) <= minDistance)
        {
            _sectionManager.SectionChange(this);
        }
    }
}
