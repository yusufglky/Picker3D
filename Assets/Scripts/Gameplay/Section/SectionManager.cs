using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectionManager : MonoBehaviour
{
    public static event Action SectionFinishedAction;
    public static event Action<Section> SectionChangeAction;
    public void SectionFinished()
    {
        SectionFinishedAction?.Invoke();
    }
    public void SectionChange(Section section)
    {
        SectionChangeAction?.Invoke(section);
    }
}
