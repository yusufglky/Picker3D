using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] private float rotSpeed;
    [SerializeField]private bool activateRotator;
    [SerializeField] private bool isLeftRotator;

    private Vector3 _startPos;
    private Rigidbody _rBody;

    private void Awake()
    {
        Setup();
    }
    private void Setup()
    {
        _startPos = transform.localPosition;
        _rBody = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        RotatorMovement();
    }
    private void RotatorMovement()
    {
        if (activateRotator)
        {
            transform.localPosition = _startPos;
            //_rBody.velocity = Vector3.zero;
            if (isLeftRotator)
            {
                _rBody.angularVelocity = new Vector3(0, rotSpeed, 0);
            }
            else
            {
                _rBody.angularVelocity = new Vector3(0, -rotSpeed, 0);
            }
        }
    }
    public void OpenRotator()
    {
        transform.DOScale(Vector3.one, 0.3f);
        activateRotator = true;
    }
    public void CloseRotator()
    {
        activateRotator = false;
    }
}
