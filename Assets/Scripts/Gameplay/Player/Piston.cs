using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piston : MonoBehaviour
{
    [SerializeField] private float pistonPumpForce;
    [SerializeField] private float pistonForce;
    [SerializeField] private List<CollectableDefault> innerCollider;
    private bool _addForce;
    private Vector3 _startPos;
    private void Awake()
    {
        Setup();
    }
    private void Update()
    {
        AddForce();
    }
    private void Setup()
    {
        _startPos = transform.localPosition;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("CollectibleObjects"))
        {
            AddObject(other.GetComponent<CollectableDefault>());
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("CollectibleObjects"))
        {
            AddObject(other.GetComponent<CollectableDefault>());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("CollectibleObjects"))
        {
            RemoveObject(other.GetComponent<CollectableDefault>());
        }
    }
    private void AddObject(CollectableDefault collectable)
    {
        if (!innerCollider.Contains(collectable))
        {
            innerCollider.Add(collectable);
        }
    }
    private void RemoveObject(CollectableDefault collectable)
    {
        if (innerCollider.Contains(collectable))
        {
            innerCollider.Remove(collectable);
        }
    }
    private void AddForce()
    {
        if (_addForce)
        {
            foreach (var item in innerCollider)
            {
                item.AddForce();
            }
            if (innerCollider.Count <= 0)
            {
                _addForce = false;
            }
        }  
    }
    public void SetInTriggerObjects()
    {
        _addForce = true;
    }
}
