using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public  class PickerBase : MonoBehaviour
{
    #region MyComponents
    private Rigidbody _rBody;
    #endregion

    [Header("PickerType")]
    [SerializeField] private PickerType pickerType;

    [Header("HorizontalClamper")]
    [SerializeField]private Vector2 minMaxHorizontal;
    [SerializeField] private Vector2 minMaxHorizontalSizeUp;

    [Header("Properties")]
    [SerializeField] private bool move;
    [SerializeField] private bool play;

    [Header("Piston")]
    [SerializeField] private Piston piston;

    [Header("Rotator")]
    [SerializeField] private GameObject rotatorBase;
    [SerializeField] private Rotator leftRotator;
    [SerializeField] private Rotator rightRotator;

    [Header("Size Up")]
    [SerializeField] private Vector3 sizeUpValue;
    [SerializeField] private bool hasSizeUp;
    public bool Move{ get { return move; } set { move = value;} }

    private void Awake()
    {
        Setup();
    }
    private void OnEnable()
    {
        SectionManager.SectionFinishedAction += SectionFinished;
        Gamemanager.PlayAction += ()=>play=true;
        Gamemanager.FinishAction += () => play = false;
    }
    private void OnDisable()
    {
        SectionManager.SectionFinishedAction -= SectionFinished;
        Gamemanager.PlayAction -= () => play = true;
        Gamemanager.FinishAction -= () => play = false;
    }
    private void Update()
    {
        MoveControl();
    }
    private void Setup()
    {
        _rBody = GetComponent<Rigidbody>();
    }
    private void MoveControl()
    {
        if (!play)
        {
            _rBody.velocity = Vector3.zero;
            return;
        }
        if (Move)
        {
            VerticalMove();
        }
        else
        {
            _rBody.velocity = Vector3.zero;
        }
        HorizontalMove();
    }
    private void HorizontalMove()
    {
        if (Input.GetMouseButton(0))
        {
            _rBody.velocity = new Vector3(Input.GetAxis("Mouse X") * pickerType.HorizontalSpeed, _rBody.velocity.y, _rBody.velocity.z);
            /*  transform.Translate(new Vector3(Input.GetAxis("Mouse X") * pickerType.HorizontalSpeed * Time.deltaTime, 0, 0));*/

            if (hasSizeUp)
            {
                minMaxHorizontal = minMaxHorizontalSizeUp;
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _rBody.velocity = new Vector3(0, 0, _rBody.velocity.z);
        }
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minMaxHorizontal.x, minMaxHorizontal.y), transform.position.y, transform.position.z);
    }
    private void VerticalMove()
    {
        //transform.Translate(Vector3.forward * pickerType.VerticalSpeed * Time.deltaTime);
        _rBody.velocity = Vector3.forward * pickerType.VerticalSpeed;
    }
    private void SetStopperProperties()
    {
        Move = false;
        _rBody.velocity = Vector3.zero;
    }
    private void SectionFinished()
    {
        if (!hasSizeUp)
        {
            hasSizeUp = true;
            SizeUp();
        }

        Move = true;
        PistonState(false);
    }
    private void ActivateRotator(GameObject rotatorActivator)
    {
        rotatorActivator.transform.DOScale(Vector3.zero, 0.3f).OnComplete(() => rotatorActivator.SetActive(false));

        rotatorBase.SetActive(true);

        leftRotator.OpenRotator();
        rightRotator.OpenRotator();
    }
    private void CloseRotator()
    {
        rotatorBase.SetActive(false);
        leftRotator.CloseRotator();
        rightRotator.CloseRotator();
    }
    private void PistonState(bool state)
    {
        if (state)
        {
            piston.SetInTriggerObjects();
        }
        else
        {
            //piston.ClosePiston();
        }
    }
    private void ColliderObjects(GameObject obj)
    {
        if (obj.CompareTag("Stopper")&&Move)
        {
            SetStopperProperties();
            PistonState(true);
            CloseRotator();
        }
    }
    private void ColliderHit(GameObject hittenObject)
    {
        if (hittenObject.GetComponent<ICollectable>() != null)
        {
            hittenObject.GetComponent<ICollectable>().Collect(transform.position);
        }
    }
    private void SizeUp()
    {
        transform.DOPunchScale(Vector3.one*0.1f, 0.3f).OnComplete(() => transform.DOScale(sizeUpValue,0.2f));
    }
    private void OnCollisionEnter(Collision collision)
    {
        ColliderObjects(collision.gameObject);
    }
    private void OnCollisionStay(Collision collision)
    {
        ColliderHit(collision.gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("RotatorActivator"))
        {
            ActivateRotator(other.gameObject);
        }
    }

}
