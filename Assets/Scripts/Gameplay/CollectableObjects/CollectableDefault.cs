using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableDefault : CollectableBase, ICollectable
{
    #region PrivateComponents
    private Rigidbody _rBody;
    #endregion

    [SerializeField] private CollectableObjectsStatics collectableObjectsStatics;

    [SerializeField] private bool forceSection;
    [SerializeField] private bool isUsed;
    private Vector3 _sectionBase = Vector3.one;
    private float _forcer = 10;
    private void Awake()
    {
        Setup();
    }
    private void OnEnable()
    {
        SectionManager.SectionChangeAction += SectionChange;
        SectionManager.SectionFinishedAction += SectionFinished;
    }
    private void OnDisable()
    {
        SectionManager.SectionChangeAction -= SectionChange;
        SectionManager.SectionFinishedAction -= SectionFinished;
    }
    private void Update()
    {
        ForceSection();
    }
    private void Setup()
    {
        _rBody = GetComponent<Rigidbody>();
    }
    private void SectionChange(Section sec)
    {
        _sectionBase = sec.MyPlatform.transform.position;
    }
    private void SectionFinished()
    {
        if (isUsed)
        {
            GetComponent<Collider>().enabled = false;
            transform.DOScale(Vector3.zero, 0.3f).OnComplete(()=>gameObject.SetActive(false));
        }
    }
    public void Collect(Vector3 forcePoint)
    {
        Vector3 forceDirection = transform.position - forcePoint;
        forceDirection.y = 0;
        forceDirection.Normalize();
        _rBody.AddForceAtPosition(forceDirection * collectableObjectsStatics.ForceMagnitude * Time.deltaTime, forcePoint, ForceMode.Impulse);
    }
    public void AddForce()
    {
         forceSection =!forceSection;
    }
    private void ForceSection()
    {
        if (forceSection)
        {
            isUsed = true;
            _rBody.AddForce((_sectionBase- transform.position)* _forcer, ForceMode.Force);
            _forcer += Time.deltaTime;
        }
    }
}
