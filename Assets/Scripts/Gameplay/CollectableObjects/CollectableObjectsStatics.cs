using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Collectable Object",menuName ="Collectable Objects/New Type")]
public class CollectableObjectsStatics : ScriptableObject
{
    public float ForceMagnitude = 1;
    public float StaticForce;
}
