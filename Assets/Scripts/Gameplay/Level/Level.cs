using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [Header("RoadColor")]
    [SerializeField] private ColorChanger roadColor;
    public ColorChanger RoadColor { get { return roadColor; } private set { roadColor = value;} }

    [SerializeField] private ColorChanger lineColor;
    public ColorChanger LineColor { get { return lineColor; } private set { lineColor = value; } }

    [Header("NextLevel")]
    [SerializeField] private Transform nextLevelT;
    public Transform NextLevelT { get { return nextLevelT; } set { nextLevelT = value; } }

    [Header("LevelStartPos")]
    [SerializeField] private Transform levelStartT;
    public Transform LevelStartT { get { return levelStartT; } set { levelStartT = value; } }
}
