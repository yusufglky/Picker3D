using System;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoSingleton<LevelManager>
{
    public static Action LevelFailedAction;
    public static Action LevelSuccessAction;

    [SerializeField] private GameObject levelGO;
    [SerializeField] private GameObject lastLevelGO;
    private Vector3 _pickerStartPos = Vector3.zero;
    [Header("StartColor")]
    [SerializeField] private ColorChanger startRoadColor;
    [SerializeField] private ColorChanger startLeftLineColor;
    [SerializeField] private ColorChanger startRightLineColor;

    private int _sectionIndex;

    [Header("Align Player To New Level")]
    [SerializeField] private bool alignPlayerNewLevel;
    [SerializeField] private Vector3 lastLevelFinishPos;

    private bool _playerAligned;
    public ColorChanger StartRoadColor { get { return startRoadColor; } private set { startRoadColor = value; } }
    private void Awake()
    {
        Setup();
    }
    private void OnEnable()
    {
        SectionManager.SectionFinishedAction += SectionFinished;
    }
    private void OnDisable()
    {
        SectionManager.SectionFinishedAction -= SectionFinished;
    }
    private void Update()
    {
        AlignPlayerNewLevel();
    }
    private void Setup()
    {
        PrepareLevel();
        StarterColor();
        _pickerStartPos = Gamemanager.Singleton.PickerTransform.position;
    }
    private void SectionFinished()
    {
        _sectionIndex++;
        if (_sectionIndex==3)
        {
            LevelSucces();
            NextLevel();
        }
    }
    private void PrepareLevel()
    {
        Debug.Log("Levels/Level" + SaveSystem.Singleton.LevelIndex.ToString());
        levelGO = Instantiate(Resources.Load("Levels/Level" + SaveSystem.Singleton.LevelIndex.ToString())as GameObject);
    }
    private void StarterColor()
    {
        Level curLevel = levelGO.GetComponent<Level>();

        StartRoadColor.NewColor(curLevel.RoadColor.MainColor, curLevel.RoadColor.HdrColor);
        startLeftLineColor.NewColor(curLevel.LineColor.MainColor, curLevel.LineColor.HdrColor);
        startRightLineColor.NewColor(curLevel.LineColor.MainColor, curLevel.LineColor.HdrColor);
    }
    private void NextLevel()
    {
        alignPlayerNewLevel = true;
        lastLevelGO = levelGO;
        levelGO = Instantiate(Resources.Load("Levels/Level" + SaveSystem.Singleton.LevelIndex.ToString()) as GameObject, levelGO.GetComponent<Level>().NextLevelT.position, Quaternion.identity);
        lastLevelFinishPos = levelGO.GetComponent<Level>().LevelStartT.position;
    }
    public void LevelSucces()
    {
        LevelSuccessAction?.Invoke();
    }
    public void LevelFailed()
    {
        LevelFailedAction?.Invoke();
    }
    private void AlignPlayerNewLevel()
    {
        if (alignPlayerNewLevel)
        {
            if (Math.Abs(Gamemanager.Singleton.PickerTransform.position.z-lastLevelFinishPos.z)<2&& !_playerAligned)
            {
                Debug.Log("Test");
                _playerAligned = true;
                lastLevelGO.SetActive(false);
                Gamemanager.Singleton.Finish();

                PreparePreLevel();
            }
        }
    }
    private void PreparePreLevel()
    {
        GameObject _levelGo = Resources.Load("Levels/Level" + SaveSystem.Singleton.LevelIndex.ToString()) as GameObject;
        levelGO.transform.position = _levelGo.transform.position;
        Gamemanager.Singleton.PickerTransform.position = _pickerStartPos;

        StarterColor();
    }
}
