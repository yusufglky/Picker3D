using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class MoverBase : MonoBehaviour
{
    [Header("Move Activator")]
    [SerializeField] private float moveActivateDistance = 20;

    [Header("Dolly Cart")]
    public CinemachineDollyCart myCart;

    
    [SerializeField] private float moveSpeed = 5;
    [SerializeField] private int pathEndId;

    public bool Move { get; private set; }
    public bool FinishedPath { get; private set; }

    private void Start()
    {
        SetStartPos();
    }
    public void Mover()
    {
        MoveActivator();
        MovePath();
        PathEnd();
    }
    private void SetStartPos()
    {
        transform.position = myCart.transform.position;
    }
    private void MoveActivator()
    {
        if (Mathf.Abs(transform.position.z - Gamemanager.Singleton.PickerTransform.position.z) < moveActivateDistance && !FinishedPath)
        {
            Move = true;
        }
    }
    private void MovePath()
    {
        if (Move)
        {
            if (myCart.m_Speed == 0)
            {
                myCart.m_Speed = moveSpeed;
            }

            transform.position = myCart.transform.position;
        }
        else
        {
            if (myCart.m_Speed == moveSpeed)
            {
                myCart.m_Speed = 0;
            }
        }
    }
    private void PathEnd()
    {
        if (Move)
        {
            if (myCart.m_Position >= pathEndId)
            {
                FinishedPath = true;

                myCart.m_Speed = 10;
            }
            if (myCart.m_Position >= myCart.m_Path.MaxPos)
            {
                Move = false;
            }
        }
    }
}
