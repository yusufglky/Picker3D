using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectBomber : MonoBehaviour
{
    [Header("Bomb Activator")]
    [SerializeField] private float bombActivateDistance = 20;
    private bool _isFinished;

    [Header("Objects")]
    [SerializeField] private GameObject objectPrefab;
    [SerializeField] private int objectCount;
    private Queue<GameObject> _objectPool;

    private Vector3 _objectsStartScale = Vector3.zero;
    private void Awake()
    {
        Setup();
    }
    private void Update()
    {
        BombActivator();
    }
    private void Setup()
    {
        objectPrefab = Resources.Load<GameObject>("ColleactableObjects/collectable" + Random.Range(1, 4));
        GetComponent<MeshFilter>().mesh = objectPrefab.GetComponent<MeshFilter>().sharedMesh; 
        gameObject.AddComponent<BoxCollider>();

        CreateObjects();
    }
    private void CreateObjects()
    {
        _objectPool = new Queue<GameObject>();
        for (int i = 0; i < objectCount; i++)
        {
            GameObject obj = Instantiate(objectPrefab);

            obj.GetComponent<ColorChanger>().NewColor(GetComponent<ColorChanger>().MainColor, GetComponent<ColorChanger>().HdrColor);
            obj.SetActive(false);

            _objectPool.Enqueue(obj);
        }
    }
    private void BombActivator()
    {
        if (Mathf.Abs(transform.position.z-Gamemanager.Singleton.PickerTransform.position.z)<bombActivateDistance&& !_isFinished)
        {
            ExploBomb();
        }
    }
    private void ExploBomb()
    {
        List<Vector3> vertices = new List<Vector3>();
        vertices.AddRange(GetComponent<MeshFilter>().mesh.vertices);
        vertices.OrderBy(x => Vector3.Distance(x, transform.position));

        bool up=false;
        int randomVertices = 0;
        Transform newObj = null;
        Vector3 thisVertexWorldPos;

        for (int i = 0; i < _objectPool.Count; i++)
        {
            newObj = _objectPool.Dequeue().transform;
            newObj.gameObject.SetActive(true);

            if (up)
            {
                randomVertices = vertices.Count - 1;
            }
            else
            {
                randomVertices = 0;
            }

            up = !up;

            thisVertexWorldPos = transform.TransformPoint(vertices[randomVertices]);
            vertices.RemoveAt(randomVertices);

            newObj.position = thisVertexWorldPos;

            //newObj.GetComponent<Rigidbody>().AddForce(Vector3.up * 2,ForceMode.Impulse);
        }
        gameObject.SetActive(false);
    }
}
