using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDisperse : MoverBase
{
    [Header("Propeller")]
    [SerializeField] private Rigidbody propellerHolder;
    [SerializeField] private float propellerSpeed = 1;

    private void Update()
    {
        Mover();
        Propeller();
    }
    private void Propeller()
    {
        propellerHolder.angularVelocity =new Vector3(0,propellerSpeed,0);
    }
}
