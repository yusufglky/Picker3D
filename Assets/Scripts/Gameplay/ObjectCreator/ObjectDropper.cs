using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class ObjectDropper : MoverBase
{
    [Header("Propeller")]
    [SerializeField] private Transform propellerHolder;
    [SerializeField] private float propellerSpeed=1;

    [Header("Objects")]
    [SerializeField] private GameObject objectPrefab;
    [SerializeField] private Transform dropPos;
    private Queue<GameObject> _objectPool;

    private Vector3 _objectsStartScale = Vector3.zero;

    [Header("DropProperties")]
    [SerializeField] private float dropTime;
    [SerializeField] private int dropCount;
    private float _dropTimer;
    private void Awake()
    {
        Setup();
    }
    private void Update()
    {
        Mover();
        Propeller();
        ObjectsDrop();
    }
    private void Setup()
    {
        _dropTimer = dropTime;

        objectPrefab = Resources.Load<GameObject>("ColleactableObjects/collectable" + Random.Range(1,4));

        CreateObjects();
    }
    private void CreateObjects()
    {
        _objectPool = new Queue<GameObject>();
        for (int i = 0; i < dropCount; i++)
        {
            GameObject obj = Instantiate(objectPrefab);
            obj.SetActive(false);
            _objectPool.Enqueue(obj);
        }
    }
    private void Propeller()
    {
        propellerHolder.Rotate(Vector3.up * 1000 * propellerSpeed * Time.deltaTime);
    }
    private void ObjectsDrop()
    {
        if (!FinishedPath &&Move)
        {
            _dropTimer += Time.deltaTime;
            if (_dropTimer>=dropTime)
            {
                _dropTimer = 0;
                Transform newObj = _objectPool.Dequeue().transform;
                newObj.gameObject.SetActive(true);

                _objectsStartScale = newObj.localScale;
                newObj.localScale = Vector3.zero;
                newObj.DOScale(_objectsStartScale, 0.3f);

                newObj.position = dropPos.position;
            }
        }
    }
}
