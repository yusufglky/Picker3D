using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Overlay : MonoBehaviour
{
    private EventSystem _eventSystem;
    [SerializeField] private float animDuration;
    [Header("MainMenu")]
    [SerializeField] private GameObject gameplayCanvas;
    [SerializeField] private GameObject dragToStart;
    
    [Header("UI")]
    [SerializeField] private TextMeshProUGUI currentLevelText;
    [SerializeField] private TextMeshProUGUI nextLevelText;
    [SerializeField] private TextMeshProUGUI emeraldText;


    [Header("SectionImage")]
    [SerializeField] private List<Image> sectionImages;
    [SerializeField] private int sectionImagesIndex;

    [SerializeField] private Color sectionCompleteColor;

    [Header("Finish")]
    [SerializeField] private RectTransform finishPanel;
    [SerializeField] private TextMeshProUGUI newLevelText;
    [SerializeField] private TextMeshProUGUI earnedEmeraldText;

    [Header("Failed")]
    [SerializeField] private RectTransform failedPanel;
    private int _startEmerald;
    private void Awake()
    {
        Setup();
    }
    private void OnEnable()
    {
        Gamemanager.PlayAction += DragToStart;
        Gamemanager.FinishAction += FinishPanel;

        SectionManager.SectionFinishedAction += NextSection;
        LevelManager.LevelFailedAction +=FailedPanel;
    }
    private void OnDisable()
    {
        Gamemanager.FinishAction -= FinishPanel;
        Gamemanager.PlayAction -=  DragToStart;
        LevelManager.LevelFailedAction -= FailedPanel;

        SectionManager.SectionFinishedAction -= NextSection;
    }
    public void ClaimButton()
    {
        finishPanel.DOScale(0, animDuration).OnComplete(() =>ReloadScene());
    }
    private void DragToStart()
    {
        dragToStart.SetActive(false);
    }
    private void Setup()
    {
        SetUI();

        _eventSystem = FindObjectOfType<EventSystem>();
        _startEmerald = SaveSystem.Singleton.Emerald;
    }
    private void SetUI()
    {
        ResetSectionImages();
        currentLevelText.text = SaveSystem.Singleton.CurrentLevel.ToString();
        nextLevelText.text = (SaveSystem.Singleton.CurrentLevel + 1).ToString();
        emeraldText.text = SaveSystem.Singleton.Emerald.ToString();
    }
    private void NextSection()
    {
        sectionImages[sectionImagesIndex].DOColor(sectionCompleteColor, 0.3f);
        if (sectionImagesIndex < sectionImages.Count - 1)
        {
            sectionImagesIndex++;
        }
        else
        {
            sectionImagesIndex = 0;
        }
    }
    private void ResetSectionImages()
    {
        for (int i = 0; i < sectionImages.Count; i++)
        {
            sectionImages[sectionImagesIndex].color = Color.white;
        }
    }
    private void FinishPanel()
    {
        EventSystemState();

        gameplayCanvas.SetActive(false);
        finishPanel.gameObject.SetActive(true);

        finishPanel.DOScale(1, animDuration).OnComplete(()=> EventSystemState());

        earnedEmeraldText.text = "+"+(SaveSystem.Singleton.Emerald - _startEmerald).ToString();
        newLevelText.text = string.Format("New Level \n -{0}-", SaveSystem.Singleton.CurrentLevel);
    }
    private void FailedPanel()
    {
        Debug.Log("Test");
        EventSystemState();

        gameplayCanvas.SetActive(false);
        failedPanel.gameObject.SetActive(true);

        failedPanel.DOScale(1, animDuration).OnComplete(() => EventSystemState());
    }
    private void EventSystemState()
    {
        _eventSystem.enabled = !_eventSystem.enabled;
    }
    public void ReloadScene()
    {
        gameplayCanvas.SetActive(true);
        SceneManager.LoadScene(0);
    }
}
