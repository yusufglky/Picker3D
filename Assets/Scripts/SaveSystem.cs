using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoSingleton<SaveSystem>
{
    [Header("Level")]
    [SerializeField] private int maxLevelIndex;
    [SerializeField] private int levelIndex=1;
    [SerializeField] private int currentLevel;

    [Header("Emerald")]
    [SerializeField] private int emerald;

    public int LevelIndex { get { return levelIndex; } private set { levelIndex = value;}}
    public int CurrentLevel { get { return currentLevel; } private set { currentLevel = value;}}
    public int Emerald { get { return emerald; } private set { emerald = value;}}

    private void Awake()
    {
        Setup();
    }
    private void OnEnable()
    {
        LevelManager.LevelSuccessAction += LevelUp;
    }
    private void OnDisable()
    {
        LevelManager.LevelSuccessAction -= LevelUp;
    }
    private void Setup()
    {
        Load();
    }
    private void Load()
    {
        if (!PlayerPrefs.HasKey("LevelIndex"))
        {
            Save();
        }

        LevelIndex = PlayerPrefs.GetInt("LevelIndex");
        CurrentLevel = PlayerPrefs.GetInt("CurrentLevel");
        Emerald = PlayerPrefs.GetInt("Emerald");
    }
    private void Save()
    {
        PlayerPrefs.SetInt("LevelIndex", LevelIndex);
        PlayerPrefs.SetInt("CurrentLevel", CurrentLevel);
        PlayerPrefs.SetInt("Emerald", Emerald);
    }
    private void LevelUp()
    {
        CurrentLevel++;

        EmeraldUp(CurrentLevel * 30);
        LevelIdChange();
        Save();
    }
    private void EmeraldUp(int emeraldCount)
    {
        Emerald += emeraldCount;
    }
    private void LevelIdChange()
    {
        LevelIndex++;
        if (LevelIndex > maxLevelIndex)
        {
            LevelIndex = 1;
        }
    }
}
