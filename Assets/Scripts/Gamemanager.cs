using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoSingleton<Gamemanager>
{
    public static event Action PlayAction;
    public static event Action FinishAction;

    private Vector2 _mouseStart = Vector2.zero;
    private Vector2 _mouseEnd = Vector2.zero;

    [Header("GameProperties")]
    [SerializeField] private bool isPlaying;

    public bool IsPlaying { get { return isPlaying; }private set { isPlaying = value; } }
    public Transform PickerTransform { get; private set;}
    private void Awake()
    {
        Setup();
    }
    private void Update()
    {
        DragToStart();
    }
    private void Setup()
    {
        PickerTransform = FindObjectOfType<PickerBase>().transform;
    }
    private void DragToStart()
    {
        if (IsPlaying)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            _mouseStart = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        }
        if (Input.GetMouseButton(0))
        {
            _mouseEnd = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            
            if (Vector2.Distance(_mouseStart, _mouseEnd) <= 3)
            {
                Play();
            }

            _mouseStart = Vector2.zero;
            _mouseEnd = Vector2.zero;

            IsPlaying = true;
        }
    }
    public void Play()
    {
        PlayAction?.Invoke();
    }
    public void Finish()
    {
        FinishAction?.Invoke();
    }
}
