using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class ColorChanger : MonoBehaviour
{
    [SerializeField]private Color mainColor;
    [ColorUsage(true, true)]
    [SerializeField] private Color hdrColor;
    public Color MainColor { get { return mainColor; }set { mainColor = value;}}
    public Color HdrColor { get { return hdrColor; } set { hdrColor = value; } }

    [SerializeField] private bool Test;
    private void Awake()
    {
        ChangeColor();
    }
    private void Update()
    {
        if (Test)
        {
            ChangeColor();
        }
    }
    private void ChangeColor()
    {
        MaterialPropertyBlock myBlock = new MaterialPropertyBlock();

        myBlock.SetColor("_BaseColor", MainColor);
        myBlock.SetColor("_EmissionColor", HdrColor);

        GetComponent<MeshRenderer>().SetPropertyBlock(myBlock);
    }
    public void NewColor(Color _mainColor, Color _hdrColor)
    {
        MainColor = _mainColor;
        HdrColor = _hdrColor;

        ChangeColor();
    }
}
